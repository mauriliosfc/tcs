<?php

namespace app\models;

use Yii;
use  yii\db\Connection;

class ConfigJobStatus extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'config_job_status';
    }

    public function rules()
    {
        return [
            [['CON_DATE', 'CON_DATE_FIM','Status'], 'safe'],
            [['CON_ID_STATUS', 'CON_INTERVALO'], 'required'],
            [['CON_ID_STATUS', 'CON_INTERVALO'], 'integer'],            
            [['CON_ID_STATUS'], 'exist', 'skipOnError' => true, 'targetClass' => StatusMaq::className(), 'targetAttribute' => ['CON_ID_STATUS' => 'STS_ID']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'CON_ID' => 'Con ID',
            'CON_DATE' => 'Início',
            'CON_ID_STATUS' => 'Status',
            'CON_INTERVALO' => 'Intervalo de tempo (em minutos)',
            'CON_DATE_FIM' => 'Executar até',
            
        ];
    }

    public function getStatus()
    {
        return $this->hasOne(StatusMaq::className(), ['STS_ID' => 'CON_ID_STATUS']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){                
            $sql="
                
                CREATE EVENT JOB_STATUS_".hash('ripemd160', $this->CON_ID_STATUS.$this->CON_INTERVALO)." 
                    ON SCHEDULE EVERY ".$this->CON_INTERVALO." MINUTE 
                        STARTS '".date("Y-m-d H:i:s")."' 
                        ENDS '".$this->CON_DATE_FIM."' ON COMPLETION PRESERVE ENABLE DO 
                        UPDATE maquinas SET MAQ_STATUS =  {$this->CON_ID_STATUS};                                
            ";
            $this->beginTransaction($sql);
        }
    }

    public function afterDelete()
    {        
        $sql="DROP EVENT IF EXISTS JOB_STATUS_".hash('ripemd160', $this->CON_ID_STATUS.$this->CON_INTERVALO).";"; 
                                                                            
        $this->beginTransaction($sql);
    }

    public function beginTransaction($sql){

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try 
        {
            $connection->createCommand($sql)->execute();               
        } 
        catch (\Exception $e) 
        {                
            throw $e;            
        }
        return true;
    }
}
