<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquina_status".
 *
 * @property string $MAQ_NOME
 * @property string $STS_CODIGO
 * @property string $STS_NOME
 * @property string $MAQ_DT_STATUS
 */
class MaquinaStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquina_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MAQ_NOME', 'STS_CODIGO', 'STS_NOME'], 'required'],
            [['MAQ_DT_STATUS'], 'safe'],
            [['MAQ_NOME'], 'string', 'max' => 50],
            [['STS_CODIGO'], 'string', 'max' => 3],
            [['STS_NOME'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [            
            'MAQ_NOME' => 'Nome',
            'STS_CODIGO' => 'Código Status',
            'STS_NOME' => 'Status',
            'MAQ_DT_STATUS' => 'Data Status',
        ];
    }
}
