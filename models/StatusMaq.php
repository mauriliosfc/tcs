<?php

namespace app\models;

use Yii;

class StatusMaq extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'status_maq';
    }

    public function rules()
    {
        return [
            [['STS_CODIGO', 'STS_NOME'], 'required'],
            [['STS_CODIGO'], 'string', 'max' => 3],
            [['STS_NOME'], 'string', 'max' => 30],
        ];
    }

    public function attributeLabels()
    {
        return [
            'STS_ID' => 'Sts ID',
            'STS_CODIGO' => 'Código',
            'STS_NOME' => 'Nome',
        ];
    }

    public function getMaquinas()
    {
        return $this->hasMany(Maquina::className(), ['MAQ_STATUS' => 'STS_ID']);
    }
}
