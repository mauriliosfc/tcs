<?php

namespace app\models;

use Yii;

class Maquinas extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'maquinas';
    }

    public function rules()
    {
        return [
            [['MAQ_NOME', 'MAQ_STATUS'], 'required'],
            [['MAQ_STATUS'], 'integer'],
            [['MAQ_NOME'], 'string', 'max' => 50],
            [['MAQ_STATUS'], 'exist', 'skipOnError' => true, 'targetClass' => StatusMaq::className(), 'targetAttribute' => ['MAQ_STATUS' => 'STS_ID']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'MAQ_ID' => 'Maq ID',
            'MAQ_NOME' => 'Nome',
            'MAQ_STATUS' => 'Status',
        ];
    }

    public function getMAQSTATUS()
    {
        return $this->hasOne(StatusMaq::className(), ['STS_ID' => 'MAQ_STATUS']);
    }

    public function getMaquinas()
    {
        return $this->find()             
                    ->orderBy('id')
                    ->all();
    }
}
