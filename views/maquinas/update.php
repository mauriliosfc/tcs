<?php

    use yii\helpers\Html;

    $this->title = 'Atualizar ' . $model->MAQ_NOME;
    $this->params['breadcrumbs'][] = ['label' => 'Maquinas', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->MAQ_ID, 'url' => ['view', 'id' => $model->MAQ_ID]];
    $this->params['breadcrumbs'][] = 'Update';
?>
<div class="maquinas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
