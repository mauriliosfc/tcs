<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\StatusMaq;
use yii\helpers\ArrayHelper;
?>

<div class="maquinas-form">
    <div class='row'>
        <div class='col-md-6'>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'MAQ_NOME')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6'>    
            <?= $form->field($model, 'MAQ_STATUS')
                    ->dropDownList(ArrayHelper::map(StatusMaq::find()->all(), 'STS_ID', 'STS_NOME')) ?>
        </div>        
        <div class="col-md-6 form-group">
            <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        </div>
    
            <?php ActiveForm::end(); ?>
        
    </div>
</div>
