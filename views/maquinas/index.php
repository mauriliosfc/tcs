<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Máquinas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Máquinas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'MAQ_NOME',                           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
