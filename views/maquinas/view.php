<?php

    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->MAQ_ID;
    $this->params['breadcrumbs'][] = ['label' => 'Maquinas', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>
<div class="maquinas-view">

    <h1><?= Html::encode($model->MAQ_NOME) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->MAQ_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->MAQ_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Deseja deletar o item selecionado?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [        
            'MAQ_NOME',
            'MAQ_STATUS',  
        ],
    ]) ?>

</div>
