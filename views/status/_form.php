<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="status-maq-form">
    <div class='row'>

        <?php $form = ActiveForm::begin(); ?>
        <div class='col-md-6'>
            <?= $form->field($model, 'STS_CODIGO')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6'>
            <?= $form->field($model, 'STS_NOME')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class=" col-md-6 form-group">
            <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
