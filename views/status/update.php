<?php

    use yii\helpers\Html;

    $this->title = 'Atualizar Status: ' . $model->STS_NOME;
    $this->params['breadcrumbs'][] = ['label' => 'Status Máquina', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->STS_ID, 'url' => ['view', 'id' => $model->STS_ID]];
    $this->params['breadcrumbs'][] = 'Update';
?>
<div class="status-maq-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
