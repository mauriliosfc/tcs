<?php

    use yii\helpers\Html;

    $this->title = 'Adicionar Status';
    $this->params['breadcrumbs'][] = ['label' => 'Status da Máquina', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-maq-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
