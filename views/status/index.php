<?php

    use yii\helpers\Html;
    use yii\grid\GridView;

    $this->title = 'Status Máquina';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-maq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Status', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'STS_CODIGO',
            'STS_NOME',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
