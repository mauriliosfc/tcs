<?php

    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->STS_NOME;
    $this->params['breadcrumbs'][] = ['label' => 'Status Máquina', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>
<div class="status-maq-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->STS_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->STS_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Deseja deletar o item selecionado?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'STS_CODIGO',
            'STS_NOME',
        ],
    ]) ?>

</div>
