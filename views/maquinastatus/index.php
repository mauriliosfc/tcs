<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maquinas Status';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                        
            'MAQ_NOME',
            'STS_CODIGO',
            'STS_NOME',
            'MAQ_DT_STATUS',                    
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
