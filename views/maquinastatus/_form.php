<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Maquinas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maquinas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'MAQ_NOME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MAQ_STATUS')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
