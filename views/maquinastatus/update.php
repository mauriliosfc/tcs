<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Maquinas */

$this->title = 'Update Maquinas: ' . $model->MAQ_ID;
$this->params['breadcrumbs'][] = ['label' => 'Maquinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->MAQ_ID, 'url' => ['view', 'id' => $model->MAQ_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maquinas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
