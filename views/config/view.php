<?php

    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = 'Configuração Job Status: '.$model->CON_ID;
    $this->params['breadcrumbs'][] = ['label' => 'Configuração Job Status', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>
<div class="config-job-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->CON_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->CON_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'CON_DATE',
            'CON_ID_STATUS',
            'CON_INTERVALO',
            'CON_DATE_FIM',                   
        ],                
    ]) ?>

</div>
