<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\StatusMaq;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker
?>

<div class="config-job-status-form">

    <div class='row'>
        <?php $form = ActiveForm::begin(); ?>

        <div class='col-md-6'>
            <?= $form->field($model, 'CON_ID_STATUS')
                    ->dropDownList(ArrayHelper::map(StatusMaq::find()->all(), 'STS_ID', 'STS_NOME')) ?>
        </div>
        <div class='col-md-6'>
            <?= $form->field($model, 'CON_INTERVALO')->textInput() ?>
        </div>
        <div class='col-md-6'>
            <?=
            
                $form->field($model, 'CON_DATE_FIM')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Informe Data e Hora'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]);
            
            ?>
                
            <div class="form-group">
                <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>        
</div>
