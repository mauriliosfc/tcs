<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Configuração de Job Status';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-job-status-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adcionar Configuração', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'CON_DATE',            
            'CON_DATE_FIM',
            'CON_INTERVALO',
            [
                'attribute' => 'status',
                'value' => 'status.STS_NOME'
            ],             

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
