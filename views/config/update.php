<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConfigJobStatus */

$this->title = 'Atualiza Configuração ' ;//. $model->CON_ID;
$this->params['breadcrumbs'][] = ['label' => 'Config Job Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CON_ID, 'url' => ['view', 'id' => $model->CON_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="config-job-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
