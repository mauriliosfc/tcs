-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.38-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para tcs
CREATE DATABASE IF NOT EXISTS `tcs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tcs`;

-- Copiando estrutura para tabela tcs.config_job_status
CREATE TABLE IF NOT EXISTS `config_job_status` (
  `CON_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CON_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `CON_ID_STATUS` int(11) NOT NULL,
  `CON_DATE_FIM` datetime DEFAULT NULL,
  `CON_INTERVALO` int(11) NOT NULL,
  PRIMARY KEY (`CON_ID`),
  KEY `CON_STATUS` (`CON_ID_STATUS`),
  CONSTRAINT `CON_STATUS` FOREIGN KEY (`CON_ID_STATUS`) REFERENCES `status_maq` (`STS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela tcs.config_job_status: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `config_job_status` DISABLE KEYS */;
INSERT INTO `config_job_status` (`CON_ID`, `CON_DATE`, `CON_ID_STATUS`, `CON_DATE_FIM`, `CON_INTERVALO`) VALUES
	(1, '2019-05-01 10:00:00', 1, '2019-05-01 13:45:00', 15),
	(3, '2019-05-01 11:05:17', 2, '2019-06-01 15:30:00', 45);
/*!40000 ALTER TABLE `config_job_status` ENABLE KEYS */;

-- Copiando estrutura para evento tcs.JOB_STATUS
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `JOB_STATUS` ON SCHEDULE EVERY 6 MINUTE STARTS '2019-05-01 11:54:59' ENDS '2019-05-31 11:54:22' ON COMPLETION PRESERVE ENABLE DO UPDATE maquinas SET 
				maquinas.MAQ_STATUS = (
			    	SELECT 
			        	STS_ID 
			        FROM config_job_status 
			        INNER JOIN status_maq ON status_maq.STS_ID = 				config_job_status.CON_ID_STATUS
			        WHERE config_job_status.CON_ID = 1
			    )//
DELIMITER ;

-- Copiando estrutura para evento tcs.JOB_STATUS2
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `JOB_STATUS2` ON SCHEDULE EVERY 1 MINUTE STARTS '2019-05-01 10:36:31' ON COMPLETION PRESERVE ENABLE DO UPDATE maquinas SET 
				maquinas.MAQ_STATUS = (
			    	SELECT 
			        	STS_ID 
			        FROM config_job_status 
			        INNER JOIN status_maq ON status_maq.STS_ID = 				config_job_status.CON_ID_STATUS
			        WHERE config_job_status.CON_ID = 3
			    )//
DELIMITER ;

-- Copiando estrutura para tabela tcs.maquinas
CREATE TABLE IF NOT EXISTS `maquinas` (
  `MAQ_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MAQ_NOME` varchar(50) NOT NULL,
  `MAQ_STATUS` int(11) NOT NULL,
  `MAQ_DT_STATUS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAQ_ID`),
  KEY `MAQ_STATUS` (`MAQ_STATUS`),
  CONSTRAINT `MAQ_STATUS` FOREIGN KEY (`MAQ_STATUS`) REFERENCES `status_maq` (`STS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela tcs.maquinas: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `maquinas` DISABLE KEYS */;
INSERT INTO `maquinas` (`MAQ_ID`, `MAQ_NOME`, `MAQ_STATUS`, `MAQ_DT_STATUS`) VALUES
	(1, 'Trator', 2, '2019-05-01 23:19:31'),
	(3, 'Escavadeira', 2, '2019-05-01 23:19:31'),
	(4, 'Colheitadeira', 2, '2019-05-01 23:19:31'),
	(5, 'RETRO', 2, '2019-05-01 23:19:31');
/*!40000 ALTER TABLE `maquinas` ENABLE KEYS */;

-- Copiando estrutura para view tcs.maquina_status
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `maquina_status` (
	`MAQ_NOME` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`STS_CODIGO` VARCHAR(3) NOT NULL COLLATE 'utf8_general_ci',
	`STS_NOME` VARCHAR(30) NOT NULL COLLATE 'utf8_general_ci',
	`MAQ_DT_STATUS` TIMESTAMP NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para tabela tcs.status_maq
CREATE TABLE IF NOT EXISTS `status_maq` (
  `STS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STS_CODIGO` varchar(3) NOT NULL,
  `STS_NOME` varchar(30) NOT NULL,
  PRIMARY KEY (`STS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela tcs.status_maq: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `status_maq` DISABLE KEYS */;
INSERT INTO `status_maq` (`STS_ID`, `STS_CODIGO`, `STS_NOME`) VALUES
	(1, 'OL', 'On Line'),
	(2, 'POL', 'Put On Line');
/*!40000 ALTER TABLE `status_maq` ENABLE KEYS */;

-- Copiando estrutura para procedure tcs.UPD_STATUS_MAQUINA
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `UPD_STATUS_MAQUINA`(
	IN `P_JOB_ID` INT
)
BEGIN
    
	UPDATE maquinas SET 
		maquinas.MAQ_STATUS = (
	    	SELECT 
	        	STS_ID 
	        FROM config_job_status 
	        INNER JOIN status_maq ON status_maq.STS_ID = config_job_status.CON_ID_STATUS
	        WHERE config_job_status.CON_ID = P_JOB_ID
	    );     
END//
DELIMITER ;

-- Copiando estrutura para view tcs.maquina_status
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `maquina_status`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `maquina_status` AS SELECT  
	MAQ_NOME,STS_CODIGO,STS_NOME,MAQ_DT_STATUS
FROM MAQUINAS
INNER JOIN STATUS_MAQ 
	ON STS_ID = MAQ_STATUS ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
